//
//  UIPlaceHolderTextView.h
//  LAB_TextView
//
//  Created by 羅祐昌 on 2016/6/24.
//  Copyright © 2016年 羅祐昌. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE

@interface UIPlaceHolderTextView : UITextView
@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;
@end
