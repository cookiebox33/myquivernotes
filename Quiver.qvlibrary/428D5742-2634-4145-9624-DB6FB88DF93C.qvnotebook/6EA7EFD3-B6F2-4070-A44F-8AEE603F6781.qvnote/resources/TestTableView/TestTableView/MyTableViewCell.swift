//
//  MyTableViewCell.swift
//  TestTableView
//
//  Created by seejobneil on 2016/12/2.
//  Copyright © 2016年 Muki. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
//    var tableView:UITableView = {
//        
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textViewDidChange(_ textView: UITextView) {
        var textViewBounds = textView.bounds
        
        //計算 textView 高度
        let maxSize = CGSize.init(width: textViewBounds.width, height: CGFloat.greatestFiniteMagnitude)
        
        let newSize = textView.sizeThatFits(maxSize)
        textViewBounds.size = newSize
        textView.bounds = textViewBounds
        
        //tableView 重新計算高度
        let tableView = getTableView()
        if (tableView.isKind(of: UITableView.self)) {
            print("真的是 tableView")
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
    func getTableView() -> UITableView {
        var tableView = self.superview
        while (!((tableView?.isKind(of: UITableView.self))!)) && (tableView != nil) {
            tableView = tableView?.superview
        }
        
        return tableView as! UITableView
        
    }
}
