//
//  ViewController.swift
//  TestTableView
//
//  Created by seejobneil on 2016/12/2.
//  Copyright © 2016年 Muki. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    
    var data = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //自適應
        tableView.estimatedRowHeight = 30
        tableView.rowHeight = UITableViewAutomaticDimension
        
        data = ["Cell1",
                "Cell2",
                "Cell3",
                "Cell4",
                "Cell5",
                "Cell6",
                "Cell7",]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! MyTableViewCell
        cell.textView.text = data[indexPath.row]
        
        
        return cell
    }
    
}

