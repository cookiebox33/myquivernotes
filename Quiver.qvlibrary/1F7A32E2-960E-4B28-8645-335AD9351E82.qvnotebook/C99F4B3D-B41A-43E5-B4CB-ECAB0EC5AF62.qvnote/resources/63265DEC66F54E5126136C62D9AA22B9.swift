//
//  UITabBarController.swift
//  SeeJob
//
//  Created by 羅祐昌 on 2017/2/14.
//  Copyright © 2017年 Muki. All rights reserved.
//

import Foundation
import UIKit

extension UITabBarController {
    /**
     帶動畫方式隱藏 / 顯示 TabBar
     - Parameters:
        - visible: true -> 顯示, false -> 隱藏
        - animated: true -> 執行動畫, false -> 不執行動畫
        - view: 執行 TabBar 隱藏/顯示的 ViewController.view
     */
    func setTabBarVisible(visible:Bool, animated:Bool, inView view: UIView) {
        
        func tabBarIsVisible() -> Bool {
            return tabBar.frame.origin.y < view.frame.maxY
        }
        
        //* This cannot be called before viewDidLayoutSubviews(), because the frame is not set before this time
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) { return }
        
        // get a frame calculation ready
        let frame = tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)
        
        // zero duration means no animation
        let duration: TimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        UIView.animate(withDuration: duration) {
            self.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
            return
        }
    }
}
