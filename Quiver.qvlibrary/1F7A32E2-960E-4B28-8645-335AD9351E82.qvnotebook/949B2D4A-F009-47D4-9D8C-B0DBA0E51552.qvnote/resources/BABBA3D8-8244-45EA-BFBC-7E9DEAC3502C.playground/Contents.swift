class Lego{
    var color = "blue"
    var size = 8
    func connect(){
        print("connect with another block")
    }
}

var oneBlock:Lego = Lego()


oneBlock.connect()


class Baby{
    var name:String
    var age:Int
    func sleep(){
        print("sleep")
    }
    func introduce(){
        print("my name is \(name)")
    }
    
    init() {
        print("a baby is born")
        name = "Thomas"
        age = 1
        introduce()
    }
    
    init(name:String){
        self.name = name
        age = 2
    }
}

class CuteBaby:Baby{
    var nickname:String
    func dance(){
        print("dance")
    }
    override func sleep() {
        print("sleep with cute smile")
        super.sleep()
    }
    
    override func introduce(){
        print("my name is \(nickname)")
    }
    
    //1.先初始化自己的屬性
    //2.呼叫 super.init
    //3.更改父類別的值
    override init(){
        nickname = "QQ"
        super.init()
        name = "Sam"
    }
}

protocol BossProtocal{
    func work()
}

class Worker:Baby,BossProtocal{
    func work() {
        print("work")
    }
}

class Boss{
    var delegate:BossProtocal?
    func askWorker(){
        delegate?.work()
    }
}

let aBoss = Boss()
let aWorker = Worker()
aBoss.delegate = aWorker
aBoss.askWorker()


func getFullName(firstName:String,lastName:String) throws ->String{
    return firstName + " " + lastName
}

do{
    let fullName = try? getFullName(firstName: "Thomas", lastName: "Wei")
}catch{
    
}






